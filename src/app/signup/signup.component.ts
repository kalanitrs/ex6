import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  email: string;
  name: string;
  password: string;
  required = "";
  wrong = "";
  result: boolean;
  code: string;
  message: string;

  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit() {
  }

  signUp() {
    this.wrong = "";
    this.result = /^[a-z0-9]+$/i.test(this.password);
    if (this.email == null || this.name == null || this.password == null) {
      this.required = "You should fill this field";
      return
    } else if (this.result) {
      this.wrong = "Password must contain a symbol ($,#, etc.)";
      return
    }
    this.authService.signUp(this.email, this.password)
    .then(value => {
      this.authService.updateProfile(value.user, this.name);
    }).then(value => {
      this.router.navigate(['/welcome']);
    }).catch(err => {
      this.code = err.code;
      this.message = err.message;
    })
  }

}
