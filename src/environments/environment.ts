// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAV2uBs-AvRAx7NQpBtFlTxd8KDlyNBw64",
    authDomain: "signup-5e119.firebaseapp.com",
    databaseURL: "https://signup-5e119.firebaseio.com",
    projectId: "signup-5e119",
    storageBucket: "signup-5e119.appspot.com",
    messagingSenderId: "1067384989930"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
